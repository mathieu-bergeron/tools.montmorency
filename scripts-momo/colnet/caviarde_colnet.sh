fichiers=$@

if [ "$fichiers" = "" ]; then
    echo ""
    echo usage $0 fichier1.csv fichier2.csv ...
    echo ""
    echo p.ex. $0 "420*.csv"
    echo ""
fi

POSITION_NUMERO_DA=3
POSITION_NUMERO_TELEPHONE=6
POSITION_MESURE_SAA=9

for fichier in $fichiers;
do
    echo "[INFO] on traite $fichier"
    cat $fichier | while read ligne_fichier
    do
        echo $ligne_fichier | grep -q ";" 
        if [ $? -eq 0 ]; then

            ligne_etudiant=$ligne_fichier

            numero_da=$(echo $ligne_etudiant | cut -d";" -f$POSITION_NUMERO_DA)
            if [ "$numero_da" != "" ]; then
                sed "s/$numero_da//g" -i $fichier
            fi

            numero_telephone=$(echo $ligne_etudiant | cut -d";" -f$POSITION_NUMERO_TELEPHONE)
            if [ "$numero_telephone" != "" ]; then
                sed "s/$numero_telephone//g" -i $fichier
            fi

            mesure_saa=$(echo $ligne_etudiant | cut -d";" -f$POSITION_MESURE_SAA)
            if [ "$mesure_saa" != "" ]; then
                sed "s/$mesure_saa//g" -i $fichier
            fi
        fi
    done
done

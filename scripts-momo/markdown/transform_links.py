#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function
import argparse
import os
import codecs
import re

parser = argparse.ArgumentParser(description='transform ciboulot $[link ]() format to ntro {{% link %}} format')
parser.add_argument('-i', metavar='IN', type=str, help='Input .md file')
parser.add_argument('-o', metavar='OUT', type=str, help='Ouput .md file')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
INPUT_PATH = args.i
OUTPUT_PATH = args.o
if OUTPUT_PATH is None:
    OUTPUT_PATH = INPUT_PATH

CIBOULOT_RE="\$\[\s*link\s+(.*)\]\s*\((.*)\)"
ciboulot_matcher=re.compile(CIBOULOT_RE)

def transform(line):
    for ciboulot_result in ciboulot_matcher.finditer(line):
        link = ciboulot_result.group(0)
        link_path = ciboulot_result.group(1)
        link_text = ciboulot_result.group(2)

        ntro_link = """{{%% link "%s" "%s" %%}}""" % (link_path, link_text)

        line = line.replace(link, ntro_link)


    return line

if __name__ == '__main__':
    output_lines = []
    with codecs.open(INPUT_PATH, encoding=ENCODING) as input_file:
        for input_line in input_file:
            output_lines.append(transform(input_line.rstrip("\n")))

    with codecs.open(OUTPUT_PATH, 'w', encoding=ENCODING) as output_file:
        output_file.write("\n".join(output_lines))

#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function
import argparse
import os
import codecs
import re

parser = argparse.ArgumentParser(description='check cloze questions for duplicate =')
parser.add_argument('-i', metavar='IN', type=str, help='Input .md file')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
INPUT_PATH = args.i

CLOZE_RE="{\d+:[^}]*}"
ANSWER_RE="="
cloze_matcher=re.compile(CLOZE_RE)
answer_matcher=re.compile(ANSWER_RE)

def check(line_number, line):
    for cloze_result in cloze_matcher.finditer(line):
        cloze_text = cloze_result.group(0)


        answer_results = answer_matcher.findall(cloze_text)

        if len(answer_results) == 0:
            print("[WARNING] line %s, no answer in %s" % (line_number, cloze_text))

        elif len(answer_results) > 1:
            print("[WARNING] line %s, %s answers in %s" % (line_number, len(answer_results), cloze_text))




if __name__ == '__main__':
    output_lines = []
    with codecs.open(INPUT_PATH, encoding=ENCODING) as input_file:
        for i, input_line in enumerate(input_file):
            check(i+1, input_line)

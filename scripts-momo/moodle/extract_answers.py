#!/usr/bin/python3

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait 
#from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

import time
import re
import os
import argparse
import codecs
import yaml

codes = []

#ID_PATTERN="[0-9]{7}"
ID_PATTERN=".*"
id_matcher = re.compile(ID_PATTERN)

parser = argparse.ArgumentParser(description='Extract answers from moodle quizz')
parser.add_argument('-d', metavar='YAML_DB', default="db.yaml", type=str, help='.yaml DB')
parser.add_argument('-m', metavar='MOODLE', type=str, help='Moodle server')
parser.add_argument('-u', metavar='USER', type=str, help='Moodle username')
parser.add_argument('-p', metavar='PASS', type=str, help='Moodle password')
parser.add_argument('-q', metavar='URL', type=str, help='quizz URL path')
parser.add_argument('-e', metavar='EXAM_NAME', type=str, help='Exam name')
parser.add_argument('--normalized-grade', metavar='NORMALIZED_GRADE', default=10.0, type=float, help='Normalized grade')
parser.add_argument('-o', metavar='OUTDIR', type=str, help='Output dir')

args = parser.parse_args()

if args.d is None or args.u is None \
   or args.p is None or args.m is None \
   or args.q is None or args.o is None \
   or args.e is None or args.normalized_grade is None:
    parser.print_usage()
    exit(0)

YAML_DB = args.d
EXAM_NAME = args.e
SERVER_NAME = args.m
QUIZZ_PATH = args.q
USERNAME = args.u
PASSWORD = args.p
OUTPUT_DIRNAME = args.o
NORMALIZED_GRADE = args.normalized_grade

MOODLE_URL = 'https://%s' % SERVER_NAME
LOGIN_URL = "%s/login/index.php" % MOODLE_URL
QUIZZ_URL = "%s/%s" % (MOODLE_URL, QUIZZ_PATH)

def replace_special_chars(value):
    if type(value) is not str:
        return value

    value = replace_accents(value)
    value = value.replace('.','')
    value = value.replace('-','')
    value = value.replace("'",'')
    value = value.replace(' ','_')

    return value

def replace_accents(value):
    if type(value) is not str:
        return value

    value = value.replace(u'é','e')
    value = value.replace(u'è','e')
    value = value.replace(u'ê','e')
    value = value.replace(u'à','a')
    value = value.replace(u'â','a')
    value = value.replace(u'î','i')
    value = value.replace(u'ô','o')
    value = value.replace(u'ë','e')
    value = value.replace(u'ï','i')

    value = value.replace(u'É','E')
    value = value.replace(u'È','E')
    value = value.replace(u'Ê','E')
    value = value.replace(u'À','A')
    value = value.replace(u'Â','A')
    value = value.replace(u'Î','I')
    value = value.replace(u'Ô','O')
    value = value.replace(u'Ë','E')
    value = value.replace(u'Ï','I')

    return value

def normalize_id(value):
    if type(value) is not str:
        return value

    value = value.lower()
    value = replace_accents(value)
    value = replace_special_chars(value)

    return value


def login(driver, login_url, username, password):
    driver.get(login_url)

    username_input = WebDriverWait(driver, 10).until(lambda x: x.find_element(By.ID, 'username')) 
    username_input.send_keys(username)

    password_input = driver.find_element(By.ID, 'password')
    password_input.send_keys(password)

    login_button = driver.find_element(By.ID, 'loginbtn')
    login_button.click()


def open_quizz_attempts(driver, quizz_url):

    print(quizz_url)
    driver.get(quizz_url)

    anchor_divs = driver.find_elements(By.CLASS_NAME,'quizattemptcounts')
    if len(anchor_divs) > 0:
        anchor_div = anchor_divs[0]
        anchors = anchor_div.find_elements(By.TAG_NAME,'a')
        if len(anchors) > 0:
            anchor = anchors[0]
            driver.get(anchor.get_attribute('href'))


MAX_GRADE = 10
MAX_GRADES = []

def extract_grade_and_answer_urls_by_student_id(driver, quizz_url):
    global MAX_GRADE
    global MAX_GRADES

    grade_and_answer_urls_by_student_id = {}

    open_quizz_attempts(driver, quizz_url)

    header_rows = driver.find_elements(By.XPATH,'//thead/tr')
    if len(header_rows) > 0:
        header_row = header_rows[0]
        cells = header_row.find_elements(By.XPATH, '//th/a')
        for cell in cells:
            cell_text = cell.text
            segments = cell_text.split("\n")
            if len(segments) > 1:
                if segments[0].startswith('Note'):
                    max_grade_text = segments[0]
                    segments = max_grade_text.split('/')
                    MAX_GRADE = segments[1]
                    MAX_GRADE = MAX_GRADE.replace(',','.')
                    try:
                        MAX_GRADE = float(MAX_GRADE)
                        print(MAX_GRADE)
                    except ValueError:
                        pass
                elif segments[0].startswith('Q'):
                    this_max_grade_text = segments[1]
                    segments = this_max_grade_text.split('/')
                    this_max_grade = segments[1]
                    this_max_grade = this_max_grade.replace(',','.')
                    try:
                        this_max_grade = float(this_max_grade)
                        MAX_GRADES.append(this_max_grade)
                        print(this_max_grade)
                    except ValueError:
                        MAX_GRADES.append(0.0)

    rows = driver.find_elements(By.XPATH,'//tbody/tr')

    for row in rows:

        # debugging on the first student
        #if len(grade_and_answer_urls_by_student_id) > 0:
            #return grade_and_answer_urls_by_student_id

        user_ids = row.find_elements(By.CLASS_NAME,'c2')
        if len(user_ids) > 0:
            user_as = user_ids[0].find_elements(By.TAG_NAME,'a')
            if len(user_as) > 0:
                moodlename = user_as[0].text
                user_id = normalize_id(moodlename)
                print(user_id)
                grade_and_answer_urls_by_student_id[user_id] = {
                                                                'grade': 0, 
                                                                'moodlename': moodlename, 
                                                                'answer_grades': [], 
                                                                'answer_urls': [],
                                                               }

                if id_matcher.match(user_id) is not None:
                    answer_number = 7

                    grade_cells = row.find_elements(By.CLASS_NAME,'c%s' % answer_number)
                    if len(grade_cells) > 0:
                        grade_cell = grade_cells[0]
                        grade = grade_cell.text
                        grade = grade.replace(',','.')
                        try:
                            grade = float(grade)
                            grade = grade / MAX_GRADE * NORMALIZED_GRADE
                            grade_and_answer_urls_by_student_id[user_id]['grade'] = grade
                        except ValueError:
                            pass
                        print(grade)

                    answer_index = 0
                    answer_number = 8
                    answer_cells = row.find_elements(By.CLASS_NAME,'c%s' % answer_number)
                    while len(answer_cells) > 0:
                        answer_cell = answer_cells[0]
                        answer_anchors = answer_cell.find_elements(By.TAG_NAME,'a')
                        if len(answer_anchors) > 0:
                            answer_anchor = answer_anchors[0]
                            answer_grade = answer_anchor.text
                            answer_grade = answer_grade.replace(',','.')
                            try:
                                answer_grade = float(answer_grade)
                                answer_max_grade = MAX_GRADES[answer_index]
                                if answer_max_grade > 0:
                                    answer_grade = answer_grade * NORMALIZED_GRADE / answer_max_grade

                                grade_and_answer_urls_by_student_id[user_id]['answer_grades'].append(answer_grade)
                            except ValueError:
                                grade_and_answer_urls_by_student_id[user_id]['answer_grades'].append(0.0)

                            answer_url = answer_anchor.get_attribute('href')
                            grade_and_answer_urls_by_student_id[user_id]['answer_urls'].append(answer_url)

                        answer_index += 1
                        answer_number += 1
                        answer_cells = row.find_elements(By.CLASS_NAME,'c%s' % answer_number)



    return grade_and_answer_urls_by_student_id

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

def extract_answers(driver, db, answers, quizz_url, output_dirname):
    grade_and_answer_urls_by_student_id = extract_grade_and_answer_urls_by_student_id(driver, quizz_url)

    for i, max_grade in enumerate(MAX_GRADES):
        if max_grade > 0:
            MAX_GRADES[i] = NORMALIZED_GRADE

    print(MAX_GRADES)

    for user_id in grade_and_answer_urls_by_student_id:

        answer_urls = grade_and_answer_urls_by_student_id[user_id]['answer_urls']
        answer_grades = grade_and_answer_urls_by_student_id[user_id]['answer_grades']

        grade = grade_and_answer_urls_by_student_id[user_id]['grade']

        current_answer = add_or_get(answers, '1', {})

        students = add_or_get(current_answer, 'students', {})

        current_student = add_or_get(students, user_id, {})

        add_or_get(current_student, 'grade', round(grade, 2))
        add_or_get(current_student, 'comment', '')
        add_or_get(current_student, 'moodlename', grade_and_answer_urls_by_student_id[user_id]['moodlename'])

        essays_path = os.path.join(output_dirname, os.path.join(user_id + ".txt"))
        current_student['_path'] = essays_path

        number_of_essays = 0
        answered_essays = 0

        for index, answer_url in enumerate(answer_urls):
            answer_name = "" + str(index + 2)
            answer_path = os.path.join(output_dirname, answer_name)
            output_path = os.path.join(answer_path, user_id + ".txt")

            if not os.path.exists(answer_path):
                os.mkdir(answer_path)

            driver.get(answer_url)
            answer_divs = driver.find_elements(By.CLASS_NAME, 'answer')

            output_text = ""
            essay_text = ""

            if len(answer_divs) > 0:
                answer_div = answer_divs[0]
                answer_text = answer_div.text
                output_text += answer_text

                essay_divs = answer_div.find_elements(By.CLASS_NAME,'qtype_essay_response')
                if len(essay_divs) > 0:
                    essay_div = essay_divs[0]
                    essay_text += essay_div.text

                    MAX_GRADES[index] = NORMALIZED_GRADE
                    number_of_essays += 1

                    if len(essay_text) > 10:
                        answered_essays += 1
                        answer_grades[index] = NORMALIZED_GRADE

                    with codecs.open(essays_path, 'a', encoding='utf8') as answers_file:
                        answers_file.write("\n\n\n=== " + answer_name + " ===\n")
                        answers_file.write(essay_text)
                        answers_file.write("\n===")

            history_divs = driver.find_elements(By.CLASS_NAME,'history')
            if len(history_divs) > 0:
                output_text += "\n\n\n# Historique"
                history_div = history_divs[0]
                rows = history_div.find_elements(By.TAG_NAME,'tr')
                for index, row in enumerate(rows):
                    if index > 1 and index != (len(rows)-1):
                        rank_cell = row.find_elements(By.CLASS_NAME,'c0')[0]
                        time_cell = row.find_elements(By.CLASS_NAME,'c1')[0]
                        action_cell = row.find_elements(By.CLASS_NAME,'c2')[0]
                        output_text += "\n\n## Action " + rank_cell.text
                        output_text += "\n" + time_cell.text
                        output_text += "\n" + action_cell.text

            with codecs.open(output_path, 'w', encoding='utf8') as output_file:
                output_file.write(output_text)

        print(answer_grades)

        new_max_grade = sum(MAX_GRADES)
        new_grade = sum(answer_grades)
        print(user_id)
        print(f"grade: {grade}")
        print(f"réponses longues: {answered_essays}/{number_of_essays}")
        print(f"with essays: {new_grade}/{new_max_grade}")
        new_grade = new_grade * NORMALIZED_GRADE / new_max_grade
        print(f"adjusted grade: {new_grade}")

        current_student['grade'] = new_grade
        current_student['comment'] = f"réponses longues: {answered_essays}/{number_of_essays}"




def dump_yaml_db(db):

    with open(YAML_DB, 'w') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db, allow_unicode=True))

    yaml_content = ""

    comment_matcher = re.compile("^(\s*)(comment:)(.*)$")

    with open(YAML_DB) as yaml_db_file:
        for yaml_line in yaml_db_file:
            comment_match = comment_matcher.match(yaml_line)
            if comment_match is not None:
                yaml_line = comment_match.group(1) + comment_match.group(2) + "\n"
                yaml_line += comment_match.group(1) + "  |\n\n"
                if comment_match.group(3) != "''":
                    comment = comment_match.group(3)
                    comment = comment.strip(" '")
                    yaml_line += comment_match.group(1) + "    " + comment + "\n\n"

            yaml_content += yaml_line

    with open(YAML_DB, 'w') as yaml_db_file:
        yaml_db_file.write(yaml_content)


if __name__ == '__main__':

    db = {}

    #if os.path.exists(YAML_DB):
        #with open(YAML_DB) as yaml_db_file:
            #db = yaml.load(yaml_db_file.read(), Loader=yaml.FullLoader)
            #if db is None:
                #db = {}

    exam = add_or_get(db, EXAM_NAME, {})

    exam['normalized_grade'] = NORMALIZED_GRADE

    answers = add_or_get(exam, 'answers', {})

    driver = webdriver.Firefox()

    login(driver, LOGIN_URL, USERNAME, PASSWORD)

    if not os.path.exists(OUTPUT_DIRNAME):
        os.makedirs(OUTPUT_DIRNAME)

    extract_answers(driver, db, answers, QUIZZ_URL, OUTPUT_DIRNAME)

    driver.close()

    dump_yaml_db(db)

#unzip *.zip

min_depth=2

sh remplacer_espaces.sh || exit

racine=$(pwd)

function extraire {
    chemin_relatif=$1
    chemin_fichier=$(readlink -f "$chemin_relatif")
    repertoire=$(dirname "$chemin_fichier")
    fichier=$(basename "$chemin_fichier")
    ext=$(echo $fichier | sed 's/\(.*\)\.\(.*\)$/\2/')
    base=$(basename -s ".$ext" "$fichier")

    if [ "$racine" = "$repertoire" ]; then
        if [ ! -e "$base" ]; then
            mkdir "$base"
        fi
        cp "$chemin_fichier" "$base"/
        cd "$base"
    else
        cd "$repertoire"
    fi

    case "$ext" in
        "zip") unzip "$fichier";;
        "rar") unrar x "$fichier";;
        "7z") 7za x "$fichier";;
    esac

    cd "$racine"

}


for i in $(find . -mindepth $min_depth -iname "*.zip" -o -iname "*.rar" -o -iname "*.7z");
do
   extraire $i;
done

#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml


parser = argparse.ArgumentParser(description='Compile grades from yaml files')
parser.add_argument('-i', metavar='IN', type=str, help='Input .yaml file')
parser.add_argument('-mi', metavar='MAX_IN', type=float, default=100, help='Input max grade ')
parser.add_argument('-mo', metavar='MAX_OUT', type=float, default=100, help='Output max grade')
parser.add_argument('-o', metavar='OUT', default="out.json", type=str, help='Output .json file')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)

MAX_GRADE_IN=args.mi
MAX_GRADE_OUT=args.mo
INPUT_PATH = args.i
OUTPUT_PATH = args.o

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value



if __name__ == '__main__':

    db = {}

    with open(INPUT_PATH) as input_file:
        db = yaml.load(input_file.read(), Loader=yaml.FullLoader)
        if db is None:
            db = {}

    students = db['presentations']

    for student in students:
        grade_in = float(students[student]['grade'])
        grade_out = grade_in/MAX_GRADE_IN*MAX_GRADE_OUT
        print(f"{student},{grade_out}")



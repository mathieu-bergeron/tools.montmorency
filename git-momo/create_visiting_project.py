#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function
import argparse
import os
import shutil
import re
import codecs

import charset_normalizer
from charset_normalizer import from_path

import fuzzywuzzy
from fuzzywuzzy import fuzz

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.clone import create_file_structure
from libmomo.clone import clone_all
from libmomo.matchers import RegexMatcher


VISITING_PROJECT_NAME='_visiting'

parser = argparse.ArgumentParser(description='Create a temporary project')
parser.add_argument('-pn', metavar='PROJECT_NAME', type=str, help='Project name')
parser.add_argument('-pp', metavar='PROJECT_PATH', type=str, help='Project path')
parser.add_argument('-wp', metavar='WORKSPACE_PATH', type=str, help='Eclipse workspace path')
parser.add_argument('-gp', metavar='GRADLE_PATH', type=str, help='Path to gradle files')
parser.add_argument('-lp', metavar='LIB_PATH', type=str, help='Search path for lib and db files')
parser.add_argument('-fn', metavar='STUDENT_FIRSTNAME', type=str, help='Student firstname')
parser.add_argument('-ln', metavar='STUDENT_LASTNAME', type=str, help='Student lastname')
parser.add_argument('-group', metavar='STUDENT_GROUP', default=None, nargs='?', type=str, help='Use a specific group in search patterns')

args = parser.parse_args()

if args.pn is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
PROJECT_NAME = args.pn
PROJECT_PATH = args.pp
WORKSPACE_PATH = args.wp
GRADLE_PATH = args.gp
LIB_PATH = args.lp
STUDENT_FIRSTNAME = args.fn
STUDENT_LASTNAME = args.ln
STUDENT_GROUP = args.group


def delete_and_recreate_eclipse_project(gradle_path, visiting_project_path):

    if os.path.exists(visiting_project_path):
        shutil.rmtree(visiting_project_path)

    shutil.copytree(gradle_path, visiting_project_path)


def find_file(root_path, filename, regexp=False):
    file_path = None

    paths_found = find_files(root_path, filename, regexp, max_results=1)
    if len(paths_found) > 0:
        file_path = list(paths_found)[0]

    return file_path

def find_files(root_path, filename, regexp=False, max_results=None):
    paths_found = set([])

    filename_matcher = lambda x : x == filename
    if regexp:
        filename_pattern = re.compile(filename, re.IGNORECASE)
        filename_matcher = lambda x : filename_pattern.match(x)

    should_stop = lambda x, y: y is not None and len(x) >= y

    for current_path, subdirs, files in os.walk(root_path):
        for candidate_filename in files:

            candidate_path = os.path.join(current_path, candidate_filename)

            if filename_matcher(candidate_filename):
                paths_found.add(candidate_path)

            if should_stop(paths_found, max_results):
                break

        if should_stop(paths_found, max_results):
            break


    return paths_found

def analyze_java_file(java_path):

    main_pattern = re.compile("^.*public.*static.*void.*main.*\(.*String.*\[.*\].*\).*$")
    package_pattern = re.compile("^.*package\s*([\w.]+);")
    class_pattern = re.compile("^.*(class|interface)\s*(\w+)")

    is_a_main_class = False
    java_class = {'path' : java_path, 'is_a_main_class' : False}

    java_content = str(from_path(java_path).best())

    for java_line in java_content.split('\n'):
        package_match = package_pattern.match(java_line)
        class_match = class_pattern.match(java_line)

        if package_match is not None:
            java_class['package'] = package_match.group(1)

        if class_match is not None:
            java_class['class'] = class_match.group(2)

        if main_pattern.match(java_line):
            is_a_main_class = True
            java_class['is_a_main_class'] = True

    return (is_a_main_class, java_class)

def analyze_java_files(java_paths, project_name):

    main_class = None
    other_classes = []

    for java_path in java_paths:
        is_a_main_class, java_class = analyze_java_file(java_path)
        is_project_main_class = False
    
        if is_a_main_class and fuzz.ratio(java_class['class'].lower(), project_name) > 50:
            is_project_main_class = True

        if is_project_main_class:

            main_class = java_class

        else:

            other_classes.append(java_class)

        filename = os.path.basename(java_path)
        basename, extension = os.path.splitext(filename)

        if basename != java_class['class']:
            print("[WARNING] wrong file name %s for class %s" % (filename, java_class['class']))

        if 'package' in java_class:
            declared_package_path = java_class['package'].replace('.', '/')
            actual_package_path = os.path.dirname(java_path)
            if not actual_package_path.endswith(declared_package_path):
                print("[WARNING] wrong path %s for package %s" % (actual_package_path, java_class['package']))

    return (main_class, other_classes)


def copy_java_files(java_classes, visiting_project_path):
    base_path = os.path.join(visiting_project_path, "src", "main", "java")

    for java_class in java_classes:
        package_path = ""
        if 'package' in java_class:
            package_path = java_class['package'].replace('.', '/')

        package_path = os.path.join(base_path, package_path)

        if not os.path.exists(package_path):
            os.makedirs(package_path)

        try:
            filepath = java_class['path']
        except KeyError as e:
            print("[FATAL] java_class has no path: " + str(java_class))
            exit()

        filename = os.path.basename(filepath)

        dst_path = os.path.join(package_path, filename)

        shutil.copyfile(filepath, dst_path)

        convert_file_to_utf8(dst_path)

def convert_file_to_utf8(filepath):

    results = from_path(filepath)

    content = str(results.best())

    with codecs.open(filepath, 'w', encoding='utf-8') as out_file:
        out_file.write(content)

def find_and_copy_file(lib_path, filename, visiting_project_path, regexp=False):

    filepath = find_file(lib_path, filename, regexp)

    actual_filename = None

    if filepath is not None:
        file_found = True
        actual_filename = os.path.basename(filepath)
        dst_path = os.path.join(visiting_project_path, actual_filename)
        shutil.copyfile(filepath, dst_path)

    return actual_filename


def update_gradle_build_files(project_name, filename_jar, main_class, visiting_project_path):

    build_path = os.path.join(visiting_project_path, "build.gradle")

    new_build_content = ""

    LIB = 'LIB'
    MAIN_CLASS = 'MAIN_CLASS'

    full_classname = ""

    if main_class is not None:
        if 'package' in main_class:
            full_classname += main_class['package'] + "."

        full_classname += main_class['class']

    with open(build_path) as build_file:
        for build_line in build_file:
            if LIB in build_line:
                if filename_jar is not None:
                    build_line = build_line.replace(LIB, filename_jar)
                else:
                    build_line = ""

            if MAIN_CLASS in build_line:
                build_line = build_line.replace(MAIN_CLASS, full_classname)

            new_build_content += build_line

    with open(build_path, 'w') as build_file:
        build_file.write(new_build_content)

def create_dummy_class(visiting_project_path, student_firstname, student_lastname):
    dummy_name = "_" + student_firstname.replace(' ', '_') + "_" + student_lastname.replace(' ', '_')
    dummy_path = os.path.join(visiting_project_path, 'src','main','java', dummy_name)

    with open(dummy_path, 'w') as dummy_file:
        dummy_file.write(student_firstname + " " + student_lastname)


if __name__ == '__main__':

    visiting_project_path = os.path.join(WORKSPACE_PATH, VISITING_PROJECT_NAME)

    delete_and_recreate_eclipse_project(GRADLE_PATH, visiting_project_path)

    jar_pattern = "^" + PROJECT_NAME + ".*"
    if STUDENT_GROUP is not None:
        jar_pattern += STUDENT_GROUP
    jar_pattern += "[.]jar$"

    db_pattern = "^" + PROJECT_NAME + ".*"
    if STUDENT_GROUP is not None:
        db_pattern += STUDENT_GROUP
    db_pattern += "[.]db$"

    actual_jarname = find_and_copy_file(LIB_PATH, jar_pattern, visiting_project_path, regexp=True)
    find_and_copy_file(LIB_PATH, db_pattern, visiting_project_path, regexp=True)

    java_paths = find_files(PROJECT_PATH, "^.*[.]java$", regexp=True)

    main_class, other_classes = analyze_java_files(java_paths, PROJECT_NAME)

    if main_class is not None:
        all_java_classes = [main_class] + other_classes
    else:
        all_java_classes = other_classes

    copy_java_files(all_java_classes, visiting_project_path)

    update_gradle_build_files(PROJECT_NAME, actual_jarname, main_class, visiting_project_path)

    create_dummy_class(visiting_project_path, STUDENT_FIRSTNAME, STUDENT_LASTNAME)

    os.chdir(visiting_project_path)

    return_code = os.system("./gradlew --offline eclipse run")

    if return_code != 0:
        os.system("bash -i")

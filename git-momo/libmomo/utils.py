#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

import os
import shutil

def initialize_dir(dirpath):
    if os.path.exists(dirpath):
        shutil.rmtree(dirpath)

    os.mkdir(dirpath)

def replace_special_chars(value):
    if value is None:
        return value

    value = replace_accents(value)
    value = value.replace('.','')
    value = value.replace('-','')
    value = value.replace("'",'')
    value = value.replace(' ','_')

    return value

def replace_accents(value):
    if value is None:
        return value

    value = value.replace(u'é','e')
    value = value.replace(u'è','e')
    value = value.replace(u'ê','e')
    value = value.replace(u'à','a')
    value = value.replace(u'â','a')
    value = value.replace(u'î','i')
    value = value.replace(u'ô','o')
    value = value.replace(u'ë','e')
    value = value.replace(u'ï','i')

    value = value.replace(u'É','E')
    value = value.replace(u'È','E')
    value = value.replace(u'Ê','E')
    value = value.replace(u'À','A')
    value = value.replace(u'Â','A')
    value = value.replace(u'Î','I')
    value = value.replace(u'Ô','O')
    value = value.replace(u'Ë','E')
    value = value.replace(u'Ï','I')

    return value

def deregexify(pattern):
    if pattern is None:
        return pattern

    pattern = pattern.replace('*','')
    pattern = pattern.replace('.','')
    pattern = pattern.replace('[','')
    pattern = pattern.replace(']','')
    pattern = pattern.replace(')','')
    pattern = pattern.replace('(','')
    pattern = pattern.replace('|','')
    pattern = pattern.replace('+','')
    pattern = pattern.replace('?','')

    return pattern

def normalize_git_url_to_ssh(git_url):
    normalized_url = normalize_git_url(git_url)

    if 'git@' not in git_url:
        normalized_url = normalized_url.replace('https://','git@')
        normalized_url = normalized_url.replace('/',':',1)

    return normalized_url;

def normalize_git_url_to_https(git_url):
    normalized_url = normalize_git_url(git_url);

    if 'http' not in git_url:
        normalized_url = normalized_url.replace(':','/',1)
        normalized_url = normalized_url.replace('git@','https://')

    return normalized_url;

def normalize_git_url(git_url):
    normalized_url = git_url

    normalized_url = normalized_url.replace('.git','')
    normalized_url = normalized_url.lower()

    return normalized_url

def normalize_project_name(project_name):
    normalized_name = project_name

    if normalized_name is not None:

        #normalized_name = normalized_name.lower()
        normalized_name = normalized_name.replace('.', '_')

    return normalized_name



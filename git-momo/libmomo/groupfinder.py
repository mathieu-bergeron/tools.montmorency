#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from libmomo.tree import Tree
from libmomo.matchers import RegexMatcher

class GroupFinder(Tree):

    def find_all_groups(self):
        return self.find_groups(RegexMatcher('.*'))

    def find_groups(self, matcher):
        for child in self.children():
            for student in child.find_groups(matcher):
                yield student

    def find_group(self, matcher):
        found = None

        for child in self.children():
            for index, group in enumerate(child.find_groups(matcher)):
                if index == 0:
                    found = group
                    break

        return found

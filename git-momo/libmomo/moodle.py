#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

import codecs

from libmomo.course import Course
from libmomo.group import Group
from libmomo.student import Student
from libmomo.repo import Repo
from libmomo.utils import normalize_git_url_to_ssh
from libmomo.utils import normalize_git_url_to_https

COURSE_POSITION=4
GROUP_POSITION=5
MOODLENAME_POSITION=7
REGISTRATION_ID_POSITION=8
REPO_URL_POSITION=9

def read_file(encoding, separator, input_path, git_url_exclude_pattern, git_url_normalize_to):

    course = None

    with codecs.open(input_path, encoding=encoding) as input_file:
        # skip first line
        input_file.readline()

        course = read_students(separator, input_file, git_url_exclude_pattern, git_url_normalize_to)

    return course

def read_students(separator, input_file, git_url_exclude_pattern, git_url_normalize_to):

    course = None

    for input_line in input_file:

        student_course = read_student_course(separator, input_line.rstrip(), git_url_exclude_pattern, git_url_normalize_to)

        if course is None:
            course = student_course
        elif not student_course is None:
            course.merge(student_course)

    return course

def read_student_course(separator, input_line, git_url_exclude_pattern, git_url_normalize_to):
    course = Course()
    group = Group()
    student = Student()
    repo = Repo()

    elements = input_line.split(separator)

    course_name = elements[COURSE_POSITION]

    moodlename = elements[MOODLENAME_POSITION].replace('"','')

    git_url = elements[9]

    course_name_elements = course_name.split('(')
    course_code_and_semesters = course_name_elements[1]
    course_code_elements = course_code_and_semesters.split('-')
    course_code = course_code_elements[0].strip()

    group_name = elements[GROUP_POSITION]
    segments = group_name.split(" ")
    group_name = segments[1]
    segments = group_name.split("-")
    group_number = segments[3]
    group_number = int(group_number)

    registration_id = "20" + elements[REGISTRATION_ID_POSITION]

    if git_url_exclude_pattern is not None and git_url_exclude_pattern in git_url:
        return None

    if git_url_normalize_to is not None and "ssh" in git_url_normalize_to.lower():
        git_url = normalize_git_url_to_ssh(git_url)
    elif git_url_normalize_to is not None and "http" in git_url_normalize_to.lower():
        git_url = normalize_git_url_to_https(git_url)

    repo.url = git_url
    student.registration_id = registration_id
    student.moodlename = moodlename
    group.number = group_number
    course.code = course_code

    student.add_or_merge_repo(repo)
    group.add_or_merge_student(student)
    course.add_or_merge_group(group)

    return course







#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml
import re

import charset_normalizer
from charset_normalizer import from_path

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import ExactMatcher

parser = argparse.ArgumentParser(description='Ajuster les notes pour examen 2')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-m', metavar='CSV_PATHS', type=str, nargs='+', help='Input .csv files')
parser.add_argument('-o', metavar='OUT', default="grading.yaml", type=str, help='Output grading file')
parser.add_argument('-e', metavar='EXAM_NAME', type=str, help='Exam name')

args = parser.parse_args()

if args.i is None or args.e is None or args.m is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
CSV_PATHS = args.m
OUTOUT_PATH = args.o
EXAM_NAME = args.e
SEPARATOR = ','
QUOTE = '"'

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value


def split_line(line, separator, quote):
    quoted_pattern = "%s([^%s]*)%s" % (quote,quote,quote)
    quoted_matcher = re.compile(quoted_pattern)

    quoted_segments = {}

    line = line.rstrip()

    for index, match in enumerate(quoted_matcher.finditer(line)):
        segment_name = str(index)
        quoted_segments[segment_name] = match.group(1)
        line = line.replace(match.group(0), segment_name)

    segments = line.split(SEPARATOR)

    for index, segment in enumerate(segments):
        if segment in quoted_segments:
            segments[index] = quoted_segments[segment]

    return segments

def note_en_float(note):
    if note == "-":
        return 0.0
    else:
        return float(note.replace(",","."))

def calculer_note(notes_questions_10_points, notes_questions_5_points):
    return sum(notes_questions_10_points) + sum(notes_questions_5_points) 

def calculer_note_max(notes_questions_10_points, notes_questions_5_points):
    note_max = len(notes_questions_10_points) * 10 + len(notes_questions_5_points) * 5
    return note_max

def ajuster_note_methode1(notes_questions_10_points, notes_questions_5_points):
    # retirer la pire note sur 10 points
    #notes_questions_10_points = notes_questions_10_points[1:]

    # retirer les deux pires notes sur 10 points
    notes_questions_10_points = notes_questions_10_points[2:]

    # somme
    note = calculer_note(notes_questions_10_points, notes_questions_5_points)
    note_max = calculer_note_max(notes_questions_10_points, notes_questions_5_points)

    return note/note_max *100

def ajuster_note_methode2(notes_questions_10_points, notes_questions_5_points):
    # retirer les deux pires notes sur 5
    notes_questions_5_points = notes_questions_5_points[2:]

    # somme
    note = calculer_note(notes_questions_10_points, notes_questions_5_points)
    note_max = calculer_note_max(notes_questions_10_points, notes_questions_5_points)

    return note/note_max *100

def ajuster_note_methode3(notes_questions_10_points, notes_questions_5_points):
    # retirer la pire note sur 10 points
    notes_questions_10_points = notes_questions_10_points[1:]

    # retirer la pire note sur 5 points
    notes_questions_5_points = notes_questions_5_points[1:]

    # somme
    note = calculer_note(notes_questions_10_points, notes_questions_5_points)
    note_max = calculer_note_max(notes_questions_10_points, notes_questions_5_points)

    return note/note_max *100

def ajuster_note_methode4(notes_questions_10_points, notes_questions_5_points):
    # retirer les 3 pires notes sur 5 points
    notes_questions_5_points = notes_questions_5_points[3:]

    # somme
    note = calculer_note(notes_questions_10_points, notes_questions_5_points)
    note_max = calculer_note_max(notes_questions_10_points, notes_questions_5_points)

    return note/note_max *100

notes_initiales = []
meilleures_notes = []

def moyenne(notes):
    return sum(notes) / len(notes)

def analyze_csv(csv_file, semester, group):
    global notes
    student_answers = {}

    for index, line in enumerate(csv_file):

        if index > 0 \
             and 'Moyenne' not in line \
             and len(line) > 0:

            segments = split_line(line, SEPARATOR, QUOTE)
            student_lastname = segments[0]
            student_firstname = segments[1]
            grade = segments[6]
            subgrades = segments[7:]

            notes_questions_10_points = subgrades[:]
            notes_questions_5_points = []
            #notes_questions_5_points = subgrades[5:]


            notes_questions_10_points = [note_en_float(x) for x in notes_questions_10_points]
            notes_questions_5_points = [note_en_float(x) for x in notes_questions_5_points]

            notes_questions_10_points.sort()
            notes_questions_5_points.sort()

            student_fullname = student_firstname + " " + student_lastname

            note_max = calculer_note_max(notes_questions_10_points, notes_questions_5_points)

            note_initiale = calculer_note(notes_questions_10_points, notes_questions_5_points)

            #print(student_fullname, note_max, notes_questions_10_points, notes_questions_5_points)

            notes = [ajuster_note_methode1(notes_questions_10_points, notes_questions_5_points), 
                     ajuster_note_methode2(notes_questions_10_points, notes_questions_5_points), 
                     ajuster_note_methode3(notes_questions_10_points, notes_questions_5_points), 
                     ajuster_note_methode4(notes_questions_10_points, notes_questions_5_points)]

            meilleure_note = max(notes)

            methode_choisie = notes.index(meilleure_note) + 1

            comment = ""

            if note_initiale < 59:
                if meilleure_note > 59:
                    meilleure_note = 59

                print(end = "methode " + str(methode_choisie) + " ")
                print(end = "=====   ")
                print(student_fullname, note_initiale, meilleure_note, "+" + str((meilleure_note - note_initiale) / meilleure_note * 100) + "%")

                comment = "note ajustée en retirant les 2 questions les moins biens réussies"

            else:
                meilleure_note = note_initiale

            notes_initiales.append(note_initiale)
            meilleures_notes.append(meilleure_note)

            student = semester.find_student(ExactMatcher(student_fullname))

            if student is None:
                print(student_fullname)
                assert(False)

            if student is not None:
                if student_fullname != student.fullname():
                    print(student.fullname())
                    print(student_fullname)
                    assert(False)

                students = add_or_get(group, 'students', {})

                student_answer = add_or_get(students, student.id(), {})

                student_answer['name'] = student_fullname
                student_answer['grade'] = meilleure_note

                student_answer['comment'] = comment


if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    db = {}

    exam = add_or_get(db, EXAM_NAME, {})

    answers = add_or_get(exam, 'answers', {})

    answer = add_or_get(answers, '1', {})

    groups = add_or_get(answer, 'groups', {})

    for index, csv_path in enumerate(CSV_PATHS):

        csv_content = str(from_path(csv_path).best())

        csv_file = csv_content.split('\n')

        group_name = "" + str(index + 1)

        group = add_or_get(groups, group_name, {})

        analyze_csv(csv_file, semester, group)


    print("")
    print("")
    print("moyenne initiale: "  + str(moyenne(notes_initiales)))
    print("moyenne ajustée: "  + str(moyenne(meilleures_notes)))

    with open(OUTOUT_PATH, 'w') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db))

    yaml_content = ""

    comment_matcher = re.compile("^(\s*)(comment:)(.*)$")

    with open(OUTOUT_PATH) as yaml_db_file:
        for yaml_line in yaml_db_file:
            comment_match = comment_matcher.match(yaml_line)
            if comment_match is not None and "''" in comment_match.group(3):
                yaml_line = comment_match.group(1) + comment_match.group(2) + "\n"
                yaml_line += comment_match.group(1) + "  |\n"

            yaml_content += yaml_line

    with open(OUTOUT_PATH, 'w') as yaml_db_file:
        yaml_db_file.write(yaml_content)


#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml
import re

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import RegexMatcher

parser = argparse.ArgumentParser(description='Merge two or more yaml grading files')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-yaml', metavar='YAML_PATHS', type=str, nargs='+', help='Input .yaml files')
parser.add_argument('-o', metavar='OUT', default="grading.yaml", type=str, help='Output grading file')
parser.add_argument('-e', metavar='EXAM_NAME', type=str, help='Exam name')

args = parser.parse_args()

if args.i is None or args.e is None or args.yaml is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
INPUT_PATHS = args.yaml
OUTOUT_PATH = args.o
EXAM_NAME = args.e

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    db = {}

    exam = add_or_get(db, EXAM_NAME, {})

    answers = add_or_get(exam, 'answers', {})

    answer = add_or_get(answers, '1', {})

    groups = add_or_get(answer, 'groups', {})

    for group_obj in semester.find_all_groups():

        group = add_or_get(groups, str(group_obj.number), {})

        students = add_or_get(group, 'students', {})

        for student_obj in semester.find_all_students():

            if student_obj.group == group_obj:

                student = add_or_get(students, student_obj.id(), {})

                grade = 0
                comment = ""
                answer_name = "1"
                max_grade = 0

                for INPUT_PATH in INPUT_PATHS:
                    max_grade += 10
                    with open(INPUT_PATH) as input_file:
                        input_db = yaml.load(input_file.read(), Loader=yaml.FullLoader)

                        for input_exam_name in input_db:

                            input_exam = input_db[input_exam_name]

                            for input_answer_name in input_exam['answers']:

                                input_answer = input_exam['answers'][input_answer_name]

                                group_name = str(group_obj.number)

                                if group_name in input_answer['groups']:

                                    group_answers = input_answer['groups'][group_name]

                                    student_id = str(student_obj.id())

                                    if student_id in group_answers['students']:

                                        student_answer = group_answers['students'][student_id]

                                        subgrade = student_answer['grade']
                                        subcomment = student_answer['comment']

                                        grade += subgrade
                                        comment += subcomment







                add_or_get(student, '_name', student_obj.fullname())
                add_or_get(student, '_answer', answer_name)
                add_or_get(student, 'grade', float("%0.2f" % (grade / max_grade * 10.0)))
                add_or_get(student, 'comment', comment)



    with open(OUTOUT_PATH, 'w') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db, allow_unicode=True))

    yaml_content = ""

    comment_matcher = re.compile("^(\s*)(comment:)(.*)$")

    with open(OUTOUT_PATH) as yaml_db_file:
        for yaml_line in yaml_db_file:
            comment_match = comment_matcher.match(yaml_line)
            if comment_match is not None and "''" in comment_match.group(3):
                yaml_line = comment_match.group(1) + comment_match.group(2) + "\n"
                yaml_line += comment_match.group(1) + "  |\n"
                #yaml_line += comment_match.group(1) + "    Tout beau!\n"

            yaml_content += yaml_line

    with open(OUTOUT_PATH, 'w') as yaml_db_file:
        yaml_db_file.write(yaml_content)

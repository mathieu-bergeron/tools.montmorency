#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml
import re
import copy

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import RegexMatcher

parser = argparse.ArgumentParser(description='Generate empty yaml grading file')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-o', metavar='OUT', default="grading.yaml", type=str, help='Output grading file')
parser.add_argument('-e', metavar='EXAM_NAME', type=str, help='Exam name')
parser.add_argument('-n', metavar='NUM_ANSWERS', type=int, help='Number of answers')
parser.add_argument('--template', metavar='TEMPLATE', type=str, help='Template file')

args = parser.parse_args()

if args.i is None or args.e is None or args.n is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
OUTOUT_PATH = args.o
EXAM_NAME = args.e
NUM_ANSWERS = args.n
TEMPLATE_PATH = args.template

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

def dump_yaml_db(db, output_path):

    with open(output_path, 'w') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db, allow_unicode=True))

    rewrite_comments(output_path)


def rewrite_comments(output_path):

    yaml_content = ""

    comment_matcher = re.compile("^(\s*)(comment:)(.*)$")
    key_matcher = re.compile("^(\s*)(\w+:)(.*)$")

    list_matcher = re.compile("^(\s*)(- '?)(.*)'?$")

    empty_comment_matcher = re.compile("'[^']'")

    in_comment = False

    with open(output_path) as yaml_db_file:

        for yaml_line in yaml_db_file:

            comment_match = comment_matcher.match(yaml_line)
            key_match = key_matcher.match(yaml_line)

            if not in_comment and comment_match:
                in_comment = True
                yaml_line = comment_match.group(1) + comment_match.group(2) + "\n"
                yaml_line += comment_match.group(1) + "  |\n\n"
                comment = comment_match.group(3)
                if empty_comment_matcher.match(comment) is None:
                    comment = comment.strip(" '")
                    yaml_line += comment_match.group(1) + "    " + comment + "\n"

                yaml_content += yaml_line

            elif in_comment and key_match is None:

                list_match = list_matcher.match(yaml_line)

                if list_match is not None:
                    indent = list_match.group(1)
                    content = list_match.group(3)
                    yaml_line = indent + "        " + content.lstrip("'").rstrip("'").replace("''''","'").replace("''","'") + "\n"
                else:
                    print(yaml_line)

                yaml_content += yaml_line


            elif in_comment and key_match:
                in_comment = False
                yaml_content += "\n" + yaml_line

            else:
                yaml_content += yaml_line
                

    with open(output_path, 'w') as yaml_db_file:
        yaml_db_file.write(yaml_content)

if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    db = {}

    exam = add_or_get(db, EXAM_NAME, {})

    answers = add_or_get(exam, 'answers', {})

    comment = []

    if TEMPLATE_PATH is not None:
        with open(TEMPLATE_PATH) as template_file:
            for template_line in template_file:
                comment.append(template_line.rstrip("\n "))


    for answer_index in range(NUM_ANSWERS):
        answer_name = "" + str(answer_index + 1)

        answer = add_or_get(answers, answer_name,{})

        groups = add_or_get(answer, 'groups', {})

        for group_obj in semester.find_all_groups():

            group = add_or_get(groups, str(group_obj.number), {})

            students = add_or_get(group, 'students', {})

            for student_obj in semester.find_all_students():

                if student_obj.group == group_obj:

                    student = add_or_get(students, student_obj.id(), {})

                    add_or_get(student, '_name', student_obj.fullname())
                    add_or_get(student, '_answer', answer_name)
                    #add_or_get(student, 'grade', 10)
                    add_or_get(student, 'grade', 0)
                    add_or_get(student, 'comment', copy.copy(comment))

    dump_yaml_db(db, OUTOUT_PATH)


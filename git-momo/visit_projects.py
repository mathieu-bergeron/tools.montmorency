#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function
import argparse
import os
import re

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.clone import create_file_structure
from libmomo.clone import clone_all
from libmomo.matchers import RegexMatcher

DEFAULT_COMMAND = 'bash -i'

parser = argparse.ArgumentParser(description='Visit projects matching some patterns')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('pp', metavar='PROJECT_PATTERN', default='.*', nargs='?', type=str, help='Project regex pattern -- defaults to .*')
parser.add_argument('sp', metavar='STUDENT_PATTERN', default='.*', nargs='?', type=str, help='Student regex pattern -- defaults to .*')
parser.add_argument('-group', metavar='USE_GROUP', default=False, nargs='?', type=bool, help='Use group in search patterns')
parser.add_argument('-c', metavar='COMMAND', default=DEFAULT_COMMAND, type=str, help='Command to visit -- defaults to "%s"' % DEFAULT_COMMAND)
parser.add_argument('--args', metavar='COMMAND_ARGS', default='', type=str, help='Extra arguments for command')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
INPUT_PATH = args.i
PROJECT_PATTERN = args.pp
STUDENT_PATTERN = args.sp
COMMAND = args.c
COMMAND_ARGS = args.args
USE_GROUP = args.group

if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, INPUT_PATH)

    project_matcher = RegexMatcher(PROJECT_PATTERN)
    student_matcher = RegexMatcher(STUDENT_PATTERN)

    for project in semester.find_projects(project_matcher, student_matcher):
        print("\nVisiting %s %s %s\n" % (project.name, project.repo.student.fullname(), project.repo.student.id()))
        os.chdir(project.localpath)
        if COMMAND == DEFAULT_COMMAND:
            os.system(COMMAND)
        else:
            student_firstname = "\"" + project.repo.student.name + "\""
            student_lastname = "\"" + project.repo.student.surname + "\""
            student_group = str(project.repo.student.group.number)

            # TMP: parce que Gr1 a fait l'examen de Gr2
            #if student_group == "1":
                #student_group = "2"

            #elif student_group is "2":
                #student_group = "1"

            arguments = ""
            arguments += " -pn " + project.name
            arguments += " -pp " + project.localpath
            arguments += " -fn " + student_firstname
            arguments += " -ln " + student_lastname
            if USE_GROUP:
                arguments += " -group " + student_group
            arguments += " " + COMMAND_ARGS

            os.system((COMMAND + " " + arguments))

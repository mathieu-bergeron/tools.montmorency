#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml
import re

import charset_normalizer
from charset_normalizer import from_path

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import ExactMatcher

parser = argparse.ArgumentParser(description='Generate empty yaml grading file')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-m', metavar='CSV_PATHS', type=str, nargs='+', help='Input .csv files')
parser.add_argument('-o', metavar='OUT', default="grading.yaml", type=str, help='Output grading file')
parser.add_argument('-e', metavar='EXAM_NAME', type=str, help='Exam name')

args = parser.parse_args()

if args.i is None or args.m is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
CSV_PATHS = args.m
OUTOUT_PATH = args.o
EXAM_NAME = args.e
SEPARATOR = ','
QUOTE = '"'

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value


def split_line(line, separator, quote):
    quoted_pattern = "%s([^%s]*)%s" % (quote,quote,quote)
    quoted_matcher = re.compile(quoted_pattern)

    quoted_segments = {}

    line = line.rstrip()

    for index, match in enumerate(quoted_matcher.finditer(line)):
        segment_name = str(index)
        quoted_segments[segment_name] = match.group(1)
        line = line.replace(match.group(0), segment_name)

    segments = line.split(SEPARATOR)

    for index, segment in enumerate(segments):
        if segment in quoted_segments:
            segments[index] = quoted_segments[segment]

    return segments

def analyze_csv(csv_file, semester, answer_name, groups_pratique, groups_theorique):

    for index, line in enumerate(csv_file):

        if index > 0 \
             and 'Moyenne' not in line \
             and len(line) > 0:

            segments = split_line(line, SEPARATOR, QUOTE)
            student_lastname = segments[0]
            student_firstname = segments[1]
            grade = segments[6]
            raw_subgrades = segments[7:]

            raw_subgrades_theorique = raw_subgrades[:6]
            raw_subgrades_pratique = raw_subgrades[6:]

            student_fullname = student_firstname + " " + student_lastname

            student = semester.find_student(ExactMatcher(student_fullname))

            if student is None:
                print(student_fullname)
                assert(False)

            if student is not None:
                if student_fullname != student.fullname():
                    print(student.fullname())
                    print(student_fullname)
                    assert(False)

                group_name = str(student.group.number)

                group_pratique = add_or_get(groups_pratique, group_name, {})
                group_theorique = add_or_get(groups_theorique, group_name, {})

                students_pratique = add_or_get(group_pratique, 'students', {})
                students_theorique = add_or_get(group_theorique, 'students', {})

                student_answer_pratique = add_or_get(students_pratique, student.id(), {})
                student_answer_theorique = add_or_get(students_theorique, student.id(), {})

                student_answer_pratique['grade'] = grade.replace(',','.')
                student_answer_theorique['grade'] = grade.replace(',','.')

                student_answer_pratique['name'] = student_fullname
                student_answer_theorique['name'] = student_fullname

                grade = 0.0

                subgrades = []

                for index,subgrade in enumerate(raw_subgrades_theorique):
                    if subgrade == '-' or subgrade == 'Nécessite évaluation':
                        subgrade = 0
                    else:
                        subgrade = float(subgrade.replace(',','.')) 
                    subgrades.append(subgrade)
                    grade += subgrade

                comment = ""

                max_grade = 6*11.11
                if grade < 0.55 * max_grade:
                    # best n-1 grades
                    old_grade = grade
                    adjusted_max_grade = max_grade - 20
                    subgrades.sort(reverse=True)
                    subgrades = subgrades[:-1]
                    grade = sum(subgrades) 
                    grade = grade / adjusted_max_grade * max_grade
                    comment = "4 meilleures questions sur 5"
                    if grade > 0.55 * max_grade:
                        comment += " (note plafonée à 55)"
                        grade = 0.55 * max_grade
                    print(old_grade, grade)

                student_answer_theorique['grade'] = float("%0.1f" % (grade * 100 / max_grade))
                student_answer_theorique['comment'] = comment

                grade = 0.0

                subgrades = []

                for index,subgrade in enumerate(raw_subgrades_pratique):
                    if subgrade == '-' or subgrade == 'Nécessite évaluation':
                        subgrade = 0
                    else:
                        subgrade = float(subgrade.replace(',','.')) 
                    subgrades.append(subgrade)
                    grade += subgrade

                comment = ""

                max_grade = 3*11.11
                if grade < 0.55 * max_grade:
                    # best n-1 grades
                    old_grade = grade
                    adjusted_max_grade = max_grade - 20
                    subgrades.sort(reverse=True)
                    subgrades = subgrades[:-1]
                    grade = sum(subgrades) 
                    grade = grade / adjusted_max_grade * max_grade
                    comment = "4 meilleures questions sur 5"
                    if grade > 0.55 * max_grade:
                        comment += " (note plafonée à 55)"
                        grade = 0.55 * max_grade
                    print(old_grade, grade)

                student_answer_pratique['grade'] = float("%0.1f" % (grade * 100.0 / max_grade))
                student_answer_pratique['comment'] = comment



def add_zero_for_each_missing_student(semester, groups):
    for student in semester.find_all_students():
        group_name = str(student.group.number)
        if group_name in groups:
            group = groups[group_name]
            students = add_or_get(group, 'students', {})
            student_id = str(student.id())
            if student_id not in students:
                print("[INFO] pas remis pour " + student.moodlename)
                student_answer = add_or_get(students, student_id, {})
                student_answer['grade'] = 0.0
                student_answer['name'] = student.moodlename
                student_answer['comment'] = "pas remis"


def commentaires(fichier_yaml):
    yaml_content = ""

    comment_matcher = re.compile("^(\s*)(comment:)(.*)$")

    with open(fichier_yaml, encoding='utf-8') as yaml_db_file:
        for yaml_line in yaml_db_file:
            comment_match = comment_matcher.match(yaml_line)
            if comment_match is not None and "''" in comment_match.group(3):
                yaml_line = comment_match.group(1) + comment_match.group(2) + "\n"
                yaml_line += comment_match.group(1) + "  |\n"

            yaml_content += yaml_line


if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    db_pratique = {}
    db_theorique = {}

    pratique = add_or_get(db_pratique, "examen5_pratique", {})
    theorique = add_or_get(db_theorique, "examen5_theorique", {})

    answers_pratique = add_or_get(pratique, 'answers', {})
    answers_theorique = add_or_get(theorique, 'answers', {})

    for index, csv_path in enumerate(CSV_PATHS):

        csv_content = str(from_path(csv_path).best())

        csv_file = csv_content.split('\n')

        answer_name = "" + str(index + 1)

        answer_pratique = add_or_get(answers_pratique, answer_name, {})
        answer_theorique = add_or_get(answers_theorique, answer_name, {})

        groups_pratique = add_or_get(answer_pratique, 'groups', {})
        groups_theorique = add_or_get(answer_theorique, 'groups', {})

        analyze_csv(csv_file, semester, answer_name, groups_pratique, groups_theorique)


    add_zero_for_each_missing_student(semester, groups_pratique)
    add_zero_for_each_missing_student(semester, groups_theorique)


    with open('pratique.yaml', 'w', encoding='utf-8') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db_pratique, allow_unicode=True))

    commentaires('pratique.yaml')

    with open('theorique.yaml', 'w', encoding='utf-8') as yaml_db_file:
        yaml_db_file.write(yaml.dump(db_theorique, allow_unicode=True))

    commentaires('theorique.yaml')

#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import ExactMatcher

parser = argparse.ArgumentParser(description='Fetch student_id from moodlename')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-yaml', metavar='YAML', type=str, help='Input .yaml file')
parser.add_argument('-o', metavar='YAML_OUT', default="out.yaml", type=str, help='Output .yaml file')

args = parser.parse_args()

if args.i is None or args.yaml is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
YAML_PATH = args.yaml
OUTOUT_PATH = args.o

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

def student_id_from_moodlename(db, semester):

    for eval_name in db:
        eval_db = db[eval_name]
        answers = eval_db['answers']
        for answer_name in answers:
            answer = answers[answer_name]
            students = answer['students']

            del answer['students']

            groups = {}
            answer['groups'] = groups



            for moodle_id in students:
                student = students[moodle_id]
                moodlename = student['moodlename']
                student_matcher = ExactMatcher(moodlename)

                db_student = semester.find_student(student_matcher)

                if db_student is None or db_student.registration_id is None:
                    raise Exception("Student not found: " + moodlename)

                student_group = db_student.group.number

                group = add_or_get(groups, student_group, {})

                group_students = add_or_get(group, 'students', {})

                group_students[db_student.registration_id] = student








if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    db = {}

    with open(YAML_PATH) as input_file:
        db = yaml.load(input_file.read(), Loader=yaml.FullLoader)
        student_id_from_moodlename(db, semester)


    with open(OUTOUT_PATH, 'w') as output_file:
        output_file.write(yaml.dump(db, allow_unicode=True))


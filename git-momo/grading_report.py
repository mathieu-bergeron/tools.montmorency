#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import RegexMatcher

parser = argparse.ArgumentParser(description='Compile grades from yaml files')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-yaml', metavar='YAML', type=str, help='Input .yaml file')
parser.add_argument('-o', metavar='OUTPUT_DIR', default="reports", type=str, help='Output dir')
parser.add_argument('-m', metavar='MAX_GRADE', default="100", type=float, help='Max grade')
parser.add_argument('-report', metavar='REPORT_NAME', default="report.yaml", type=str, help='Report name')

args = parser.parse_args()

if args.i is None or args.yaml is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
INPUT_PATH = args.yaml
OUTPUT_DIR = args.o
REPORT_PATH = args.report
MAX_GRADE   = args.m

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    db = {}

    with open(INPUT_PATH) as input_file:
        db = yaml.load(input_file.read(), Loader=yaml.FullLoader)
        if db is None:
            db = {}

    reports = {}
    student_names = {}
    max_grades = {}


    for exam_name in db:

        exam = db[exam_name]

        answers = exam['answers']

        for answer_name in answers:

            answer = answers[answer_name]

            groups = answer['groups']

            for group_number in groups:

                group = groups[group_number]

                students = group['students']

                for student_id in students:

                    student_answer = students[student_id]

                    student = semester.find_student(RegexMatcher(student_id))

                    if student is not None:

                        student_names[student_id] = student.fullname()

                        exam_reports = add_or_get(reports, exam_name, {})

                        group_reports = add_or_get(exam_reports, "{group:02d}".format(group=student.group.number), {})

                        report = add_or_get(group_reports, student_id, {})

                        add_or_get(report, answer_name, student_answer)

                        this_max_grade = len(report) * 10

                        if exam_name not in max_grades \
                            or (exam_name in max_grades and max_grades[exam_name] < this_max_grade):

                                max_grades[exam_name] = this_max_grade

                    
                        




    grades = {}

    for exam_name in reports:

        exam_reports = reports[exam_name]

        for group_name in exam_reports:

            group_grades = add_or_get(grades, group_name, {})

            group_reports = exam_reports[group_name]

            group_path = os.path.join(OUTPUT_DIR, exam_name, group_name)

            if not os.path.exists(group_path):
                os.makedirs(group_path)

            for student_id in group_reports:

                report = group_reports[student_id]

                report_path = os.path.join(group_path, student_id + '.txt')

                total_grade = float(0)
                report_text = ""
                student_grades = []

                for answer_name in report:
                    answer = report[answer_name]
                    grade = float(answer['grade'])
                    student_grades.append(grade)

                    #report_text += "## Question " + "{answer_name:s}".format(answer_name=answer_name) + ") " + "{grade:0.1f}".format(grade=grade) + "/10"
                    #report_text += "## Mini-test " + "{answer_name:s}".format(answer_name=answer_name) + ") " + "{grade:0.1f}".format(grade=grade) + "/10"
                    #report_text += "## Atelier " + "{answer_name:s}".format(answer_name=answer_name) + ") " + "{grade:0.1f}".format(grade=grade) + "/10"
                    #report_text += "\n\n"
                    report_text += "\n"
                    report_text += answer['comment']
                    report_text += "\n\n"

                    total_grade += grade

                #max_grade = max_grades[exam_name]
                max_grade=MAX_GRADE

                # best n-1 grades
                #max_grade = max_grade - 10
                #student_grades.sort(reverse=True)
                #student_grades = student_grades[:-1]
                #total_grade = sum(student_grades) 

                final_grade = "{grade:0.2f}/{max_grade:0.0f}".format(grade=total_grade, max_grade = max_grade)
                #final_grade = "{grade:0.2f}/{max_grade:0.0f}".format(grade=total_grade/max_grade*10, max_grade = 10)
                #final_grade = "{grade:0.2f}/{max_grade:0.0f}".format(grade=total_grade, max_grade = 100)
                #final_grade = "{grade:0.2f}/{max_grade:0.0f} (2 meilleurs des 3)".format(grade=total_grade, max_grade = max_grade)
                #final_grade = "{grade:0.2f}".format(grade=total_grade)

                student_grades = add_or_get(group_grades, student_id, {})

                student_grades['_name'] = student_names[student_id]
                #student_grades['grade'] = round(float(total_grade)/max_grade*10,2)

                if (type(total_grade) is float or type(total_grade) is int) and total_grade >= 0:
                    student_grades['grade'] = round(float(total_grade),2)
                else:
                    student_grades['grade'] = 'évaluation annulée'

                student_grades['path'] = report_path

                with open(report_path, 'w') as report_out:

                    report_out.write("# Note: " + final_grade + "\n\n")

                    report_out.write(report_text)

        with open(REPORT_PATH, 'w') as report_file:
            report_file.write(yaml.dump(grades, allow_unicode=True))


#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

from __future__ import print_function

import os
import argparse
import yaml

from libmomo import jsonio
from libmomo.semester import Semester
from libmomo.matchers import RegexMatcher

parser = argparse.ArgumentParser(description='Compile grades from yaml files')
parser.add_argument('-i', metavar='IN', default='db.json', type=str, help='Input .json file -- defaults to db.json')
parser.add_argument('-yaml', metavar='YAML', type=str, nargs='+', help='Input .yaml files')
parser.add_argument('-o', metavar='YAML_OUT', default="out.yaml", type=str, help='Output .yaml file')

args = parser.parse_args()

if args.i is None or args.yaml is None:
    parser.print_usage()
    exit(0)

ENCODING = 'utf-8'
JSON_PATH = args.i
INPUT_PATHS = args.yaml
OUTOUT_PATH = args.o

def add_or_get(_dict, key, value):
    final_value = value

    if key in _dict:
        final_value = _dict[key]
    else:
        _dict[key] = final_value

    return final_value

def merge_db_keep_max_grades(db1, db2):
    for evaluation in db1:
        print(evaluation)
        eval1 = db1[evaluation]
        eval2 = db2[evaluation]

        answers1 = add_or_get(eval1,'answers',{})
        answers2 = add_or_get(eval2,'answers',{})

        for answer_name in answers1:
            answer1 = add_or_get(answers1,answer_name,{})
            answer2 = add_or_get(answers2,answer_name,{})

            groups1 = add_or_get(answer1,'groups',{})
            groups2 = add_or_get(answer2,'groups',{})

            for group in groups1:
                group1 = add_or_get(groups1,group,{})
                group2 = add_or_get(groups2,group,{})

                students1 = add_or_get(group1,'students',{})
                students2 = add_or_get(group2,'students',{})

                for student_id in students1:
                    student1 = add_or_get(students1,student_id,{})
                    student2 = add_or_get(students2,student_id,{})

                    student1_grade = add_or_get(student1,'grade',0)
                    student2_grade = add_or_get(student2,'grade',0)

                    student1_comment = add_or_get(student1,'comment','')
                    student2_comment = add_or_get(student2,'comment','')

                    student1_name = add_or_get(student1,'name','')
                    student2_name = add_or_get(student2,'name','')

                    if student2_grade > student1_grade:
                        print("%s +%s (%s %s)" % (student1_name, (student2_grade - student1_grade), student1_grade, student2_grade))
                        student1['grade'] = student2_grade

                    if len(student2_name) > len(student1_name):
                        student1['name'] = student2_name

                    if len(student2_comment) > len(student1_comment):
                        student1['comment'] = student2_comment




    return db1

if __name__ == '__main__':

    semester = jsonio.read_file(ENCODING, JSON_PATH)

    dbs = []

    for INPUT_PATH in INPUT_PATHS:
        with open(INPUT_PATH) as input_file:
            db = yaml.load(input_file.read(), Loader=yaml.FullLoader)
            dbs.append(db)

    db = {}

    for db1 in dbs:
        for db2 in dbs:
            db = merge_db_keep_max_grades(db1,db2)

    with open(OUTOUT_PATH, 'w') as output_file:
        output_file.write(yaml.dump(db, allow_unicode=True))

